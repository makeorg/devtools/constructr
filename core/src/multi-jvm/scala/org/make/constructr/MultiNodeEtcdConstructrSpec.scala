/*
 * Copyright 2015 Make.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.make.constructr

import akka.actor.{ Address, AddressFromURIString }
import io.circe.Json
import io.circe.parser.parse
import java.util.Base64

class MultiNodeEtcdConstructrSpecMultiJvmNode1 extends MultiNodeEtcdConstructrSpec(startEtcd = true)
class MultiNodeEtcdConstructrSpecMultiJvmNode2 extends MultiNodeEtcdConstructrSpec(startEtcd = false)
class MultiNodeEtcdConstructrSpecMultiJvmNode3 extends MultiNodeEtcdConstructrSpec(startEtcd = false)
class MultiNodeEtcdConstructrSpecMultiJvmNode4 extends MultiNodeEtcdConstructrSpec(startEtcd = false)
class MultiNodeEtcdConstructrSpecMultiJvmNode5 extends MultiNodeEtcdConstructrSpec(startEtcd = false)

object MultiNodeEtcdConstructrSpec {
  def toNodes(s: String): Set[Address] = {
    def jsonToNode(json: Json): Address = {
      val key =
        json.hcursor
          .get[String]("key")
          .fold(throw _, identity)
          .stripPrefix("/constructr/MultiNodeConstructrSpec/nodes/")
      AddressFromURIString(new String(Base64.getUrlDecoder.decode(key)))
    }
    parse(s)
      .fold(throw _, identity)
      .hcursor
      .downField("node")
      .get[Set[Json]]("nodes")
      .getOrElse(Set.empty)
      .map(jsonToNode)
  }
}

abstract class MultiNodeEtcdConstructrSpec(startEtcd: Boolean)
    extends MultiNodeConstructrSpec(
      delete = "/v2/keys/constructr?recursive=true",
      get = "/v2/keys/constructr/MultiNodeConstructrSpec/nodes",
      toNodes = MultiNodeEtcdConstructrSpec.toNodes,
      startEtcd = startEtcd
    )
