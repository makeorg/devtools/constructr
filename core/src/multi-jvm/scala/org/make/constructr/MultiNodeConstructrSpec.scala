/*
 * Copyright 2015 Make.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.make.constructr

import akka.actor.{Actor, Address, Props}
import akka.cluster.{Cluster, ClusterEvent}
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.model.StatusCodes.{NotFound, OK}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.pattern.ask
import akka.remote.testkit.{MultiNodeConfig, MultiNodeSpec}
import akka.stream.ActorMaterializer
import akka.testkit.TestDuration
import akka.util.Timeout
import com.github.dockerjava.core.{DefaultDockerClientConfig, DockerClientConfig}
import com.github.dockerjava.netty.NettyDockerCmdExecFactory
import com.typesafe.config.ConfigFactory
import com.whisk.docker.impl.dockerjava.{Docker, DockerJavaExecutorFactory}
import com.whisk.docker.{DockerContainer, DockerFactory, DockerKit, DockerReadyChecker}
import org.scalatest.{BeforeAndAfterAll, FreeSpecLike, Matchers}

import scala.concurrent.Await
import scala.concurrent.duration.{DurationInt, FiniteDuration}

class ConstructrMultiNodeConfig extends MultiNodeConfig {

  commonConfig(ConfigFactory.load())
  for (n <- 1.to(5)) {
    val port = 2550 + n
    nodeConfig(role(port.toString))(
      ConfigFactory.parseString(
        s"""|akka.actor.provider            = akka.cluster.ClusterActorRefProvider
          |akka.remote.netty.tcp.hostname = "127.0.0.1"
          |akka.remote.netty.tcp.port     = $port
          |constructr.coordination.host   = "localhost"
          |constructr.coordination.port   = 2381
          |""".stripMargin
      )
    )
  }
}

abstract class MultiNodeConstructrSpec(
    delete: String,
    get: String,
    toNodes: String => Set[Address],
    startEtcd: Boolean
) extends MultiNodeSpec(new ConstructrMultiNodeConfig)
    with FreeSpecLike
    with Matchers
    with BeforeAndAfterAll
    with DockerKit {

  import RequestBuilding._

  private val dockerClientConfig: DockerClientConfig =
    DefaultDockerClientConfig.createDefaultConfigBuilder().build()

  private val client: Docker                         = new Docker(dockerClientConfig, new NettyDockerCmdExecFactory())
  override implicit val dockerFactory: DockerFactory = new DockerJavaExecutorFactory(client)

  override val StartContainersTimeout: FiniteDuration = 20.seconds
  override val StopContainersTimeout: FiniteDuration  = 10.minutes

  private def etcdContainer: DockerContainer =
    DockerContainer(image = "microbox/etcd:latest", name = Some(getClass.getSimpleName))
      .withPorts(2381 -> Some(2381))
      .withReadyChecker(
        DockerReadyChecker.LogLineContains("leader changed from '' to 'constructr'")
      )
      .withCommand("-name", "constructr", "-addr", "localhost:2381")

  override def dockerContainers: List[DockerContainer] =
    if (startEtcd) {
      etcdContainer :: super.dockerContainers
    } else {
      super.dockerContainers
    }

  implicit val mat: ActorMaterializer = ActorMaterializer()

  "Constructr should manage an Akka cluster" in {
    runOn(roles.head) {
      within(20.seconds.dilated) {
        awaitAssert {
          val coordinationStatus =
            Await.result(
              Http()
                .singleRequest(
                  Delete(s"http://localhost:2381$delete")
                )
                .map(_.status),
              5.seconds.dilated // As this is the first request fired via `singleRequest`, creating the pool takes some time (probably)
            )
          coordinationStatus should (be(OK) or be(NotFound))
        }
      }
    }

    enterBarrier("coordination-started")

    ConstructrExtension(system)

    class TestActor extends Actor {
      import ClusterEvent._

      var isMember = false

      Cluster(context.system).subscribe(self,
        InitialStateAsEvents,
        classOf[MemberJoined],
        classOf[MemberUp])

      def receive: Receive = {
        case "isMember" => sender() ! isMember

        case MemberJoined(member) if member.address == Cluster(context.system).selfAddress =>
          isMember = true

        case MemberUp(member) if member.address == Cluster(context.system).selfAddress =>
          isMember = true
      }
    }

    val listener = system.actorOf(Props(new TestActor))
    within(20.seconds.dilated) {
      awaitAssert {
        implicit val timeout: Timeout = Timeout(1.second.dilated)
        val isMember         = Await.result((listener ? "isMember").mapTo[Boolean], 1.second.dilated)
        isMember shouldBe true
      }
    }

    enterBarrier("cluster-formed")

    within(5.seconds.dilated) {
      awaitAssert {
        val constructrNodes =
          Await.result(
            Http()
              .singleRequest(
                Get(s"http://localhost:2381$get")
              )
              .flatMap(Unmarshal(_).to[String].map(toNodes)),
            1.second.dilated
          )
        val ports = constructrNodes.flatMap(_.port)
        ports shouldBe roles.map(_.name.toInt).toSet
      }
    }

    enterBarrier("done")
  }

  override def initialParticipants: Int = roles.size

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    startAllOrFail()
    multiNodeSpecBeforeAll()
  }

  override protected def afterAll(): Unit = {
    stopAllQuietly()
    multiNodeSpecAfterAll()
    super.afterAll()
  }
}
