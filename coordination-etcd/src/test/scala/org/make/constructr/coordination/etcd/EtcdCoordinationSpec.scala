/*
 * Copyright 2015 Make.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.make.constructr.coordination.etcd

import akka.Done
import akka.actor.{ ActorSystem, AddressFromURIString }
import akka.testkit.{ TestDuration, TestProbe }
import com.github.dockerjava.core.{ DefaultDockerClientConfig, DockerClientConfig }
import com.github.dockerjava.netty.NettyDockerCmdExecFactory
import com.typesafe.config.ConfigFactory
import com.whisk.docker.impl.dockerjava.{ Docker, DockerJavaExecutorFactory }
import com.whisk.docker.{ DockerContainer, DockerFactory, DockerKit, DockerReadyChecker }
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpecLike }

import scala.concurrent.duration.{ Duration, DurationInt, FiniteDuration }
import scala.concurrent.{ Await, Awaitable }
import scala.util.Random

class EtcdCoordinationSpec
    extends DockerKit
    with WordSpecLike
    with Matchers
    with BeforeAndAfterAll {

  private val dockerClientConfig: DockerClientConfig =
    DefaultDockerClientConfig.createDefaultConfigBuilder().build()

  private val client: Docker                         = new Docker(dockerClientConfig, new NettyDockerCmdExecFactory())
  override implicit val dockerFactory: DockerFactory = new DockerJavaExecutorFactory(client)

  override val StartContainersTimeout: FiniteDuration = 20.seconds
  override val StopContainersTimeout: FiniteDuration  = 10.minutes

  private def etcdContainer: DockerContainer =
    DockerContainer(image = "microbox/etcd:latest", name = Some(getClass.getSimpleName))
      .withPorts(2379 -> Some(2379))
      .withReadyChecker(
        DockerReadyChecker.LogLineContains("leader changed from '' to 'constructr'")
      )
      .withCommand("-name", "constructr", "-addr", "localhost:2379")

  override def dockerContainers: List[DockerContainer] =
    etcdContainer :: super.dockerContainers

  private implicit val system = {
    val config =
      ConfigFactory
        .parseString(s"constructr.coordination.host = localhost")
        .withFallback(ConfigFactory.load())
    ActorSystem("default", config)
  }

  private val address  = AddressFromURIString("akka.tcp://default@a:2552")
  private val address2 = AddressFromURIString("akka.tcp://default@b:2552")

  "EtcdCoordination" should {
    "correctly interact with etcd" in {
      val coordination = new EtcdCoordination(randomString(), system)

      resultOf(coordination.getNodes()) shouldBe Symbol("empty")

      resultOf(coordination.lock(address, 10.seconds.dilated)) shouldBe true
      resultOf(coordination.lock(address, 10.seconds.dilated)) shouldBe true
      resultOf(coordination.lock(address2, 10.seconds.dilated)) shouldBe false

      resultOf(coordination.addSelf(address, 10.seconds.dilated)) shouldBe Done
      resultOf(coordination.getNodes()) shouldBe Set(address)

      resultOf(coordination.refresh(address, 1.second.dilated)) shouldBe Done
      resultOf(coordination.getNodes()) shouldBe Set(address)

      val probe = TestProbe()
      probe.within(5.seconds.dilated) { // 2 seconds should be enough, but who knows hows ...
        probe.awaitAssert {
          resultOf(coordination.getNodes()) shouldBe Symbol("empty")
        }
      }
    }
  }

  override protected def afterAll() = {
    Await.ready(system.terminate(), Duration.Inf)
    stopAllQuietly()
    super.afterAll()
  }

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    startAllOrFail()
  }

  private def resultOf[A](awaitable: Awaitable[A], max: FiniteDuration = 3.seconds.dilated) =
    Await.result(awaitable, max)

  private def randomString() = math.abs(Random.nextInt).toString
}
