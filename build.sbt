// *****************************************************************************
// Projects
// *****************************************************************************

lazy val constructr =
  project
    .in(file("."))
    .enablePlugins(GitVersioning)
    .aggregate(core, coordination, `coordination-etcd`)
    .settings(settings)
    .settings(
      unmanagedSourceDirectories.in(Compile) := Seq.empty,
      unmanagedSourceDirectories.in(Test) := Seq.empty,
      publishArtifact := false
    )

lazy val core =
  project
    .enablePlugins(AutomateHeaderPlugin)
    .configs(MultiJvm)
    .dependsOn(coordination, `coordination-etcd` % "test->compile")
    .settings(settings)
    .settings(multiJvmSettings)
    .settings(
      name := "constructr",
      libraryDependencies ++= Seq(
        library.akkaCluster,
        library.akkaMultiNodeTestkit % Test,
        library.akkaTestkit          % Test,
        library.log4jCore            % Test,
        library.mockitoCore          % Test,
        library.scalaTest            % Test,
        library.dockerClient         % Test,
        library.dockerScalatest      % Test
      )
    )

lazy val coordination =
  project
    .enablePlugins(AutomateHeaderPlugin)
    .settings(settings)
    .settings(
      name := "constructr-coordination",
      libraryDependencies ++= Seq(
        library.akkaActor
      )
    )

lazy val `coordination-etcd` =
  project
    .enablePlugins(AutomateHeaderPlugin)
    .dependsOn(coordination)
    .settings(settings)
    .settings(
      name := "constructr-coordination-etcd",
      libraryDependencies ++= Seq(
        library.akkaHttp,
        library.akkaStream,
        library.circeParser,
        library.akkaTestkit     % Test,
        library.dockerClient    % Test,
        library.dockerScalatest % Test,
        library.scalaTest       % Test
      )
    )

// *****************************************************************************
// Library dependencies
// *****************************************************************************

lazy val library =
  new {
    object Version {
      final val akka      = "2.5.25"
      final val akkaHttp  = "10.1.10"
      final val circe     = "0.12.1"
      final val log4j     = "2.9.1"
      final val mockito   = "2.24.5"
      final val scalaTest = "3.0.8"
    }
    val akkaActor            = "com.typesafe.akka"        %% "akka-actor"                      % Version.akka
    val akkaCluster          = "com.typesafe.akka"        %% "akka-cluster"                    % Version.akka
    val akkaHttp             = "com.typesafe.akka"        %% "akka-http"                       % Version.akkaHttp
    val akkaMultiNodeTestkit = "com.typesafe.akka"        %% "akka-multi-node-testkit"         % Version.akka
    val akkaSlf4j            = "com.typesafe.akka"        %% "akka-slf4j"                      % Version.akka
    val akkaStream           = "com.typesafe.akka"        %% "akka-stream"                     % Version.akka
    val akkaTestkit          = "com.typesafe.akka"        %% "akka-testkit"                    % Version.akka
    val circeParser          = "io.circe"                 %% "circe-parser"                    % Version.circe
    val log4jCore            = "org.apache.logging.log4j" % "log4j-core"                       % Version.log4j
    val mockitoCore          = "org.mockito"              % "mockito-core"                     % Version.mockito
    val scalaTest            = "org.scalatest"            %% "scalatest"                       % Version.scalaTest
    val dockerScalatest      = "com.whisk"                %% "docker-testkit-scalatest"        % "0.9.9"
    val dockerClient         = "com.whisk"                %% "docker-testkit-impl-docker-java" % "0.9.9"
  }

// *****************************************************************************
// Settings
// *****************************************************************************        |
val scala213 = "2.13.1"
val scala212 = "2.12.10"

lazy val settings =
commonSettings ++
gitSettings ++
scalafmtSettings ++
publishSettings ++
multiJvmSettings

lazy val commonSettings =
  Seq(
    scalaVersion := scala213,
    crossScalaVersions := Seq(scala213, scala212),
    organization := "org.make.constructr",
    organizationName := "Make.org",
    startYear := Some(2015),
    licenses += ("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0")),
    scalacOptions ++= Seq(
      "-unchecked",
      "-deprecation",
      "-language:_",
      "-target:jvm-1.8",
      "-encoding",
      "UTF-8"
    ),
    unmanagedSourceDirectories.in(Compile) := Seq(scalaSource.in(Compile).value),
    unmanagedSourceDirectories.in(Test) := Seq(scalaSource.in(Test).value)
  )

lazy val gitSettings =
  Seq(
    git.useGitDescribe := true
  )

lazy val scalafmtSettings =
  Seq(
    scalafmtOnCompile := true,
    scalafmtOnCompile.in(Sbt) := false,
    scalafmtVersion := "1.3.0"
  )

lazy val publishSettings =
  Seq(
    homepage := Some(url("https://gitlab.com/makeorg/devtools/constructr")),
    scmInfo := Some(
      ScmInfo(
        browseUrl = url("https://gitlab.com/makeorg/devtools/constructr"),
        connection = "scm:git:git://gitlab.com:makeorg/devtools/constructr.git",
        devConnection = Some("scm:git:ssh://gitlab.com:makeorg/devtools/constructr.git")
      )
    ),
    developers += Developer("flaroche",
                            "François LAROCHE",
                            "fl@make.org",
                            url("https://github.com/larochef")),
    pomIncludeRepository := (_ => false),
    publishMavenStyle := true,
    releasePublishArtifactsAction := PgpKeys.publishSigned.value,
    pgpPassphrase := {
      val password: String = System.getenv("GPG_PASSWORD")
      password match {
        case null =>
          None
        case "" =>
          None
        case other =>
          Some(other.trim.toCharArray)
      }
    },
    publishTo := {
      if (isSnapshot.value) {
        Some("releases".at("https://oss.sonatype.org/content/repositories/snapshots"))
      } else {
        Some("snapshots".at("https://oss.sonatype.org/service/local/staging/deploy/maven2"))
      }
    }
  )

lazy val multiJvmSettings =
com.typesafe.sbt.SbtMultiJvm.multiJvmSettings ++
inConfig(MultiJvm)(scalafmtSettings) ++
headerSettings(MultiJvm) ++
automateHeaderSettings(MultiJvm) ++
Seq(
  unmanagedSourceDirectories.in(MultiJvm) := Seq(scalaSource.in(MultiJvm).value),
  test.in(Test) := test.in(MultiJvm).dependsOn(test.in(Test)).value
)

addCommandAlias("run-tests", s";++${scala213};clean;test;++${scala212};clean;test")