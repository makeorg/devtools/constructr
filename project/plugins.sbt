addSbtPlugin("com.lucidchart"    % "sbt-scalafmt"  % "1.14")
addSbtPlugin("com.typesafe.sbt"  % "sbt-git"       % "0.9.3")
addSbtPlugin("com.typesafe.sbt"  % "sbt-multi-jvm" % "0.4.0")
addSbtPlugin("de.heikoseeberger" % "sbt-header"    % "5.2.0")
addSbtPlugin("com.github.gseitz" % "sbt-release"   % "1.0.9")
addSbtPlugin("com.jsuereth"      % "sbt-pgp"       % "1.1.1")

classpathTypes += "maven-plugin"


libraryDependencies += "org.slf4j" % "slf4j-nop" % "1.7.25"  // Needed by sbt-git
